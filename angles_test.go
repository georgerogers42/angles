package angles

import (
	"testing"
)

func assert(t *testing.T, b bool, s, f string) {
	if b {
		t.Log(s)
	} else {
		t.Error(f)
	}
}

func TestSin1(t *testing.T) {
	var y PercentTurn
	SetRadians(&y, Tau/4)
	assert(t, ToDegrees(y) == 90, "Good", "What")
}
func TestSin2(t *testing.T) {
	var y Turn
	SetRadians(&y, Tau/4)
	assert(t, ToDegrees(y) == 90, "Good", "What")
}
func TestSin3(t *testing.T) {
	var y Gradians
	SetRadians(&y, Tau/4)
	assert(t, ToPercentTurn(y) == 25, "Good", "What")
}
func TestSin4(t *testing.T) {
	var y Degrees
	SetRadians(&y, Tau/4)
	assert(t, ToTurn(y) == 0.25, "Good", "What")
}
