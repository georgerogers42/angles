package angles

import (
	"fmt"
	"math"
)

const Tau = math.Pi * 2

type Angle interface {
	String() string
	Radians() float64
}
type PAngle interface {
	Angle
	SetRadians(x float64)
}

type Radians float64

func (r Radians) String() string {
	return fmt.Sprintf("%f radians", float64(r))
}
func (r Radians) Radians() float64 {
	return float64(r)
}
func (r *Radians) SetRadians(x float64) {
	*r = Radians(x)
}

type Turn float64

func (r Turn) String() string {
	if r > 1 {
		return fmt.Sprintf("%f turns", float64(r))
	} else {
		return fmt.Sprintf("%f turn", float64(r))
	}
}
func (p Turn) Radians() float64 {
	return float64(p * Tau)
}
func (p *Turn) SetRadians(x float64) {
	*p = Turn(x / Tau)
}

type PercentTurn float64

func (r PercentTurn) String() string {
	return fmt.Sprintf("%f%% turn", float64(r))
}
func (p PercentTurn) Radians() float64 {
	return float64((p / 100) * Tau)
}
func (p *PercentTurn) SetRadians(x float64) {
	*p = PercentTurn((x / Tau) * 100)
}

type Gradians float64

func (r Gradians) String() string {
	return fmt.Sprintf("%f gradians", float64(r))
}
func (g Gradians) Radians() float64 {
	return float64((g / 400) * Tau)
}
func (p *Gradians) SetRadians(x float64) {
	*p = Gradians((x / Tau) * 400)
}

type Degrees float64

func (r Degrees) String() string {
	return fmt.Sprintf("%f degrees", float64(r))
}
func (g Degrees) Radians() float64 {
	return float64((g / 360) * Tau)
}
func (p *Degrees) SetRadians(x float64) {
	*p = Degrees((x / Tau) * 360)
}

func Sin(x Angle) float64 {
	return math.Sin(x.Radians())
}
func Cos(x Angle) float64 {
	return math.Sin(x.Radians())
}
func Tan(x Angle) float64 {
	return math.Sin(x.Radians())
}
func Asin(x PAngle, s float64) {
	x.SetRadians(math.Asin(s))
}
func Acos(x PAngle, s float64) {
	x.SetRadians(math.Acos(s))
}
func Atan(x PAngle, s float64) {
	x.SetRadians(math.Atan(s))
}

func SetRadians(a PAngle, r float64) {
	a.SetRadians(r)
}
func Convert(d PAngle, s Angle) {
	SetRadians(d, s.Radians())
}
func ToDegrees(s Angle) Degrees {
	var d Degrees
	Convert(&d, s)
	return d
}
func ToTurn(s Angle) Turn {
	var d Turn
	Convert(&d, s)
	return d
}
func ToPercentTurn(s Angle) PercentTurn {
	var d PercentTurn
	Convert(&d, s)
	return d
}
func ToGradians(s Angle) Gradians {
	var d Gradians
	Convert(&d, s)
	return d
}
func ToRadians(s Angle) Radians {
	var d Radians
	Convert(&d, s)
	return d
}
